.PHONY: package-clean package clean

package-clean:
	rm -rf pkg/ sssvt-prumery-znamek-*.zip

package: package-clean
	cp -r src/ pkg/
	zip -j -r sssvt-prumery-znamek.zip pkg/
	rm -rf pkg/

clean: package-clean
