# SSŠVT - průměry známek
## Not maintained
As I have graduated from SSŠVT, I no longer have access to the website. Therefore, this project is no longer maintained. Feel free to send merge requests.

## Firefox
https://addons.mozilla.org/en-GB/firefox/addon/ssšvt-průměry-známek/

## Chrome
https://chrome.google.com/webstore/detail/dnggnihajjjbifbpofnbafhcdcmaggnb

## License
The contents of this repository are provided under the GPLv3 license or any later version (SPDX identifier: `GPL-3.0-or-later`).
