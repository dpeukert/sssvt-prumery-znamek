document.head.innerHTML += "<style>.sssvt-prumer{color: red}</style>";

document.querySelectorAll('.znamky').forEach(subject => {
    let marks = subject.querySelectorAll("tbody tr .large");
    let markcount = marks.length;
    let marksum = 0;
    let ncount = 0;

    if(markcount === 0) return;

    marks.forEach(mark => {
        let markvalue = mark.innerHTML.trim().replace("-",".5");

        if(isNaN(markvalue)) {
            if(markvalue === "N") ncount++;
            markcount -= 1;
        } else {
            marksum += +markvalue;
        }
    });

    subject.querySelector('thead .nazev').innerHTML += ' - <span class="sssvt-prumer">' + (marksum / markcount).toFixed(3) + (ncount > 0 ? " (některé ze známek jsou N)" : "") + '</span>';
});
